﻿using ProjectenII.Models.Domain;
using System.Web.Mvc;

namespace ProjectenII.Infrastructure
{
    public class PromotorModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IGebruikerRepository repos = (IGebruikerRepository)DependencyResolver.Current.GetService(typeof(IGebruikerRepository));
                return repos.FindBy(controllerContext.HttpContext.User.Identity.Name);
            }
            return null;
        }

      
    }
}