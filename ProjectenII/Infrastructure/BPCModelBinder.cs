﻿using System.Web.Mvc;
using ProjectenII.Models.Domain;

namespace ProjectenII.Infrastructure
{
    public class BPCModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IGebruikerRepository repos = (IGebruikerRepository)DependencyResolver.Current.GetService(typeof(IGebruikerRepository));
                return repos.FindBy(controllerContext.HttpContext.User.Identity.Name);
            }
            return null;
        }


    }
}