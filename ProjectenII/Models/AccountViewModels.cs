﻿using System.ComponentModel.DataAnnotations;

namespace ProjectenII.Models
{

    public class WijzigWachtwoordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Huidig wachtwoord")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Het {0} moet minimum {2} karakters lang zijn.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nieuw wachtwoord")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Bevestig nieuw wachtwoord")]
        [Compare("NewPassword", ErrorMessage = "De bevestiging van je wachtwoord komt niet overeen met je nieuwe wachtwoord")]
        public string ConfirmPassword { get; set; }

        public string GebruikersNaam { get; set; }
    }

    public class NieuwWachtwoordModel
    {
        [Required]
        [Display(Name = "Gebruikersnaam")]
        public string GebruikersNaam { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Gebruikersnaam")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Wachtwoord")]
        public string Password { get; set; }
    }

}
