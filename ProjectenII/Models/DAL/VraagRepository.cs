﻿using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.DAL
{
    public class VraagRepository : IVraagRepository
    {
        private BachelorProefContext context;
        private DbSet<Vraag> vragen;

        public VraagRepository(BachelorProefContext context)
        {
            this.context = context;
            this.vragen = context.Vragen;
        }
        public IQueryable<Vraag> FindAll()
        {
            return vragen.OrderBy(c => c.Id);
        }

        public Vraag FindBy(int id)
        {
            return vragen.Find(id);
        }
    }
}