﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using ProjectenII.Models.Domain;
using WebGrease.Css.Extensions;

namespace ProjectenII.Models.DAL
{

    public class BachelorProefInitializer : DropCreateDatabaseAlways<BachelorProefContext>
    {

        // Initializer is verleden tijd, maar laten we hem staan, wie weet komt ie ooit nog van pas

        protected override void Seed(BachelorProefContext context)
        {
            try
            {


                Campus campusGent = new Campus("Gent");
                Campus campusAalst = new Campus("Aalst");

                BPC bpc = new BPC("123456dw", "Wiedadde", "docteur", "tecqmenne_glenn@hotmail.com", "e6550f233edab61979983bd74c97b519", false, campusGent);
                BPC bpc2 = new BPC("654321dw", "Wiedadde", "docteur", "DocteurWiedadde@hogent.be", "e6550f233edab61979983bd74c97b519", false, campusAalst);

                context.Onderzoeksdomeinen.Add(new Onderzoeksdomein(11, "Bugdetectie"));
                context.Onderzoeksdomeinen.Add(new Onderzoeksdomein(22, "Youtube"));
                context.Onderzoeksdomeinen.Add(new Onderzoeksdomein(33, "Nutteloze apps"));
                context.Onderzoeksdomeinen.Add(new Onderzoeksdomein(44, "Designpatterns"));

                Promotor promotor1 = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "e6550f233edab61979983bd74c97b519", false, campusGent);
                Promotor promotor2 = new Promotor("12345cf", "Figis", "Cyril", "CyrilFigis@hogent.be", "e6550f233edab61979983bd74c97b519", false, campusGent);
                Promotor promotor3 = new Promotor("12345ma", "Archer", "Malory", "MaloryArcher@hogent.be", "e6550f233edab61979983bd74c97b519", false, campusGent);
                Promotor promotor4 = new Promotor("12345ct", "Tunt", "Carol", "CarolTunt@hogent.be", "e6550f233edab61979983bd74c97b519", false, campusAalst);

                Student student1 = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "e6550f233edab61979983bd74c97b519", false, promotor1, campusGent);
                Student student2 = new Student("12345jv", "Vermeulen", "Jos", "JosVermeulen@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor1, campusGent);
                Student student3 = new Student("12346km", "Mertens", "Koen", "KoenMertens@student.hogent.be", "e6550f233edab61979983bd74c97b519", false, promotor1, campusGent);
                Student student4 = new Student("12345ss", "Schepens", "Sam", "SamSchepens@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor2, campusGent);
                Student student5 = new Student("12346bp", "Pintjes", "Bram", "BramPintjes@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor2, campusGent);
                Student student6 = new Student("12345is", "Smaele", "Ian", "IanSmaele@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor2, campusAalst);
                Student student7 = new Student("12345sh", "Hoskens", "Sven", "SvenHoskens@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor3, campusAalst);
                Student student8 = new Student("12345mp", "Parmentier", "Maarten", "MaartenParmentier@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor3, campusAalst);
                Student student9 = new Student("12345gt", "Tecqmenne", "Glenn", "GlennTecqmenne@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor3, campusAalst);
                Student student10 = new Student("12345sk", "Keymeulen", "Sander", "SanderKeymeulen@student.hogent.be", "e6550f233edab61979983bd74c97b519", true, promotor4, campusGent);
                Student student11 = new Student("12345gd", "Donders", "Geoffrey", "GeoffreyDonders@student.hogent.be", "e6550f233edab61979983bd74c97b519", false, promotor4, campusGent);


                IList<Onderzoeksdomein> onderzoeksdomeinenList = new List<Onderzoeksdomein>();
                onderzoeksdomeinenList.Add(new Onderzoeksdomein(4, "Studie der NVidia's"));
                onderzoeksdomeinenList.Add(new Onderzoeksdomein(5, "IvanDeVerschrikkelijke-ologie"));

                // Voorstellen instellen bij enkele studenten
                student1.StelVoorstelIn("Het leven van de computerbug", onderzoeksdomeinenList, "trefwoorden", "probleemstelling", "onderzoeksvraag", "planVanAanpak",
                "referentielijst", "copromotorNaam", "copromotorEmail");
                student2.StelVoorstelIn("Hoe ik een miljardenbizz zal beginnen die handelt in cyborgkonijnen", onderzoeksdomeinenList, "trefwoorden", "Sinds ik een " +
                "kleine uk was, is het mijn droom geweest om een cyborkonijnen kweker te worden en hiermee stinkend rijk te worden." +
                 " Verder hou ik erg van me te verkleden als een vampier, dus konijnen met rode ogen hebben mijn voorkeur." +
                 " Hoe ik deze moet kweken en programmeren om zo mijn vampierkonijnonderdanen te worden om zo te wereld te onderwerpen, is hetgene dat ik ga onderzoeken in deze bachelorproef. Dank.", "onderzoeksvraag", "planVanAanpak",
                "referentielijst", "copromotorNaam", "copromotorEmail");
                student3.StelVoorstelIn("Waarom ik geen website kan maken zoals Mark Zuckerberg", onderzoeksdomeinenList, "trefwoorden", "probleemstelling", "onderzoeksvraag", "planVanAanpak",
                "referentielijst", "copromotorNaam", "copromotorEmail");

                Vraag vraag1 = new Vraag() { Id = 1, Text = "Beschrijft de titel specifiek wat het onderwerp van de scriptie is?" };
                Vraag vraag2 = new Vraag() { Id = 2, Text = "Is de context duidelijk beschreven en worden vaktermen uitgelegd?" };
                Vraag vraag3 = new Vraag() { Id = 3, Text = "Zijn de doelstellingen specifiek, meetbaar, aanvaardbaar, realistisch en tijdsgebonden?" };
                Vraag vraag4 = new Vraag() { Id = 4, Text = "Is de onderzoeksvraag geïnspireerd door een reëel probleem uit het werkveld?" };
                Vraag vraag5 = new Vraag() { Id = 5, Text = "Is het duidelijk wat de bijdrage van de studie zal zijn?" };
                Vraag vraag6 = new Vraag() { Id = 6, Text = "Is het onderwerp voldoende uitdagend en haalbaar?" };
                Vraag vraag7 = new Vraag() { Id = 7, Text = "Is er minstens 1 goede bron opgenomen in de referentielijst?" };

                student1.Voorstel.LikertSchaal = "middelmatig,slecht,middelmatig,slecht,middelmatig,slecht,middelmatig";
                student1.Voorstel.ToAdviesState();
                student1.Voorstel.VraagAanBPC = "Ik heb een vraag voor u, beste man.";
                student2.Voorstel.LikertSchaal = "middelmatig,slecht,middelmatig,slecht,middelmatig,slecht,middelmatig";
                student2.Voorstel.ToAdviesState();
                student2.Voorstel.VraagAanBPC = "Hoe Lang is een chinees.";
                student3.Voorstel.ToInBehandelingState();
               
 
                promotor1.Studenten.Add(student1);
                promotor1.Studenten.Add(student2);
                promotor1.Studenten.Add(student3);
                promotor2.Studenten.Add(student4);
                promotor2.Studenten.Add(student5);
                promotor2.Studenten.Add(student6);
                promotor3.Studenten.Add(student7);
                promotor3.Studenten.Add(student8);
                promotor3.Studenten.Add(student9);
                promotor4.Studenten.Add(student10);
                promotor4.Studenten.Add(student11);

                var studenten = new List<Student> 
                {
                    student1, student2, student3, student4, student5, student6, student7,student8,student9,student10,student11
                };

                var promotoren = new List<Promotor>
                {
                    promotor1, promotor2, promotor3, promotor4
                };

                var vragen = new List<Vraag>
                {
                    vraag1, vraag2, vraag3, vraag4, vraag5, vraag6, vraag7
                };

                vragen.ForEach(c => context.Vragen.Add(c));
                studenten.ForEach(c => context.Gebruikers.Add(c));
                promotoren.ForEach(c => context.Gebruikers.Add(c));
                context.Gebruikers.Add(bpc);
                context.Gebruikers.Add(bpc2);
                onderzoeksdomeinenList.ForEach(c => context.Onderzoeksdomeinen.Add(c));

                context.SaveChanges();
            }

            catch (DbEntityValidationException e)
            {
                string message = String.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    message +=
                        String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                      eve.Entry.Entity.GetType().Name, eve.Entry.GetValidationResult());
                    foreach (var ve in eve.ValidationErrors)
                    {
                        message +=
                      String.Format("- Property: \"{0}\", Error: \"{1}\"",
                          ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new ApplicationException("Fout bij aanmaken database " + message);
            }

        }
    }

}