﻿using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.DAL
{
    public class StudentRepository : IStudentRepository
    {
        private BachelorProefContext context;
        private DbSet<Student> studenten;

        public StudentRepository(BachelorProefContext context)
        {
            this.context = context;
            //studenten = context.Studenten;
        }

        public IQueryable<Student> FindAll()
        {
            return studenten.OrderBy(l => l.GebruikersNaam);
        }

        public Student FindBy(string naam)
        {
            return studenten.Find(naam);
        }

        public Student FindByPromotor(string naam)
        {
            //return studenten.FirstOrDefault(c => c.PromotorNaam == naam);
            return studenten.Find(naam);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

    }
}