﻿using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.DAL
{
    public class OnderzoeksdomeinRepository : IOnderzoeksdomeinRepository
    {
        private BachelorProefContext context;

        private DbSet<Onderzoeksdomein> onderzoeksdomeinen;

        public OnderzoeksdomeinRepository(BachelorProefContext context)
        {
            this.context = context;
            onderzoeksdomeinen = context.Onderzoeksdomeinen;
        }

        public IQueryable<Onderzoeksdomein> FindAll()
        {
            return onderzoeksdomeinen.OrderBy(l => l.Naam);            
        }

        public Onderzoeksdomein FindBy(int id)
        {
            return onderzoeksdomeinen.Find(id);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}