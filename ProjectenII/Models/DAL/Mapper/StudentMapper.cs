﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;


namespace ProjectenII.Models.DAL.Mapper
{
    public class StudentMapper : EntityTypeConfiguration<Student>
    {
        public StudentMapper()
        {
            //HasRequired(c => c.Promotor).WithMany();
            
            HasOptional(c => c.Promotor).WithMany();

            HasOptional(t => t.Voorstel).WithRequired().Map(m => m.MapKey("GebruikersNaam"));





            

        }
    }
}