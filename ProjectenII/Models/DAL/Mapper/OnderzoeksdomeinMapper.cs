﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.Models.DAL.Mapper
{
    public class OnderzoeksdomeinMapper : EntityTypeConfiguration<Onderzoeksdomein>
    {
        public OnderzoeksdomeinMapper()
        {

            //HasKey(p => p.OnderzoeksdomeinId);
            //Property(b => b.Naam).HasMaxLength(40).IsRequired();

            ToTable("Onderzoeksdomein");
        }
    }
}