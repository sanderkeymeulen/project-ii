﻿using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.DAL.Mapper
{
    public class VraagMapper : EntityTypeConfiguration<Vraag>
    {
        public VraagMapper()
        {
            HasKey(b=>b.Id);
            ToTable("Vraag");
        }
    }
}