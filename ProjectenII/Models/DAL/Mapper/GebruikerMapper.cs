﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.Models.DAL.Mapper
{
    public class GebruikerMapper : EntityTypeConfiguration<Gebruiker>
    {
        public GebruikerMapper()
        {

            HasKey(b => b.GebruikersNaam);
            ToTable("Gebruiker");

        }
    }
}