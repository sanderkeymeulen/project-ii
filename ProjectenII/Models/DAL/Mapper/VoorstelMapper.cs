﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.Models.DAL.Mapper 
{
    public class VoorstelMapper: EntityTypeConfiguration<Voorstel>
    {
        public VoorstelMapper()
        {
            HasMany(b => b.Onderzoeksdomeinen).WithMany().Map(
                m =>
                {
                    m.ToTable("VoorstelOnderzoeksdomein");
                    m.MapLeftKey("Voorstelnummer");
                    m.MapRightKey("OnderzoeksdomeinId"); 
                });

            HasKey(b => b.Voorstelnummer);

            ToTable("Voorstel");

        }
    }
}