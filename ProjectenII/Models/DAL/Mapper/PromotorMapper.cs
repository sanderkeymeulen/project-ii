﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.Models.DAL.Mapper
{
    public class PromotorMapper : EntityTypeConfiguration<Promotor>
    {
        public PromotorMapper()
        {
            
            HasMany(r => r.Studenten).WithRequired(r => r.Promotor).Map(t => t.MapKey("PromotorNaam"));
           
            
            
        }
    }
}