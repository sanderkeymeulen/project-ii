﻿using System.Data.Common;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using MySql.Data.Entity;
using ProjectenII.Models.DAL.Mapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.Models.DAL
{
    public class ApplicationUser : IdentityUser
    {
    }

    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class BachelorProefContext : DbContext
    {

       

        public BachelorProefContext() : base("bachelorproef")
        {

        }

        public DbSet<Gebruiker> Gebruikers { get; set; }
        public DbSet<Voorstel> Voorstellen { get; set; }
        public DbSet<Vraag> Vragen { get; set; }
        public DbSet<Onderzoeksdomein> Onderzoeksdomeinen { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new GebruikerMapper());
            modelBuilder.Configurations.Add(new VoorstelMapper());
            modelBuilder.Configurations.Add(new StudentMapper());
            modelBuilder.Configurations.Add(new PromotorMapper());
            modelBuilder.Configurations.Add(new OnderzoeksdomeinMapper());
            modelBuilder.Configurations.Add(new VraagMapper());
        }
    }

    public class ApplicationDbContext : IdentityDbContext<Models.ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}