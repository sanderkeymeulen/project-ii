﻿using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.DAL
{
    public class GebruikerRepository : IGebruikerRepository
    {
        private BachelorProefContext context;
        private DbSet<Gebruiker> gebruikers;

        public GebruikerRepository(BachelorProefContext context)
        {
            this.context = context;
            gebruikers = context.Gebruikers;
        }

        public IQueryable<Gebruiker> FindAll()
        {
            return gebruikers.OrderBy(l => l.Naam);
            
        }

        public IQueryable<Gebruiker> FindAllStudenten()
        {    
            return gebruikers.OfType<Student>();
        }

        public IQueryable<Promotor> FindAllPromotoren()
        {
            return gebruikers.OfType<Promotor>();
        }

        public IQueryable<Gebruiker> FindAllBPCs()
        {
            return gebruikers.OfType<BPC>();
        }

        public Gebruiker FindBy(string naam)
        {
            return gebruikers.Find(naam);
        }

        public void Add(Gebruiker gebruiker)
        {
            gebruikers.Add(gebruiker);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}