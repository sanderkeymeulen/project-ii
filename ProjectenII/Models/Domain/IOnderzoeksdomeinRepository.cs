﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public interface IOnderzoeksdomeinRepository
    {
        IQueryable<Onderzoeksdomein> FindAll();
        Onderzoeksdomein FindBy(int naam);
        void SaveChanges();
    }
}