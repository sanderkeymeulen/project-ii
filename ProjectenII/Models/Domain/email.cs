﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Configuration;

namespace ProjectenII.Models.Domain
{
    public class Email
    {
        public static void stuurEmail(EmailHulp emailHulp)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpClient = new SmtpClient(WebConfigurationManager.AppSettings["mailserverAdres"]);
                
                //Setting From , To and CC
                mail.From = new MailAddress(WebConfigurationManager.AppSettings["emailVoorMeldingen"]);
                foreach (string adres in emailHulp.OntvangersList)
                {
                    mail.To.Add(new MailAddress(adres));
                }
                mail.Subject = emailHulp.Onderwerp;
                mail.Body = emailHulp.Boodschap + "\n\n\n Dit is een automatisch opgestelde mail. Gelieve hier niet op te antwoorden.";

                smtpClient.Port = Convert.ToInt32(WebConfigurationManager.AppSettings[" emailPoort"]);
                smtpClient.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["emailVoorMeldingen"],
                WebConfigurationManager.AppSettings["wachtwoordEmailServer"]);
                smtpClient.EnableSsl = true;

                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class EmailHulp
    {
        public List<string> OntvangersList { get; set; }
        public string Boodschap { get; set; }
        public string Onderwerp { get; set; }
        public string NaamPromotor { get; set; }
        public string EmailStudent { get; set; }
        public string VraagAanBPC { get; set; }
        public string Opmerking { get; set; }
        public string TitelBp { get; set; }
        public string EmailBPC { get; set; }

        public EmailHulp()
        {
            
        }
        public EmailHulp(string naamPromotor, string emailPromotor, string naamStudent, string emailStudent, string emailBPC, string vraagAanBPC, string opmerking, string titelBp, string naarState)
        {
            NaamPromotor = naamPromotor;
            EmailStudent = emailStudent;
            VraagAanBPC = vraagAanBPC;
            Opmerking = opmerking;
            TitelBp = titelBp;
            EmailBPC = emailBPC;
            OntvangersList = new List<string>();

            switch (naarState)
            {
                case "VraagAanBPC" :
                    OntvangersList.Add(emailBPC);
                    Onderwerp = "Vraag van " + naamPromotor + " over bachelorproef van "+ naamStudent;
                    Boodschap = "Er werd een vraag gesteld door "+ naamPromotor + "(" + emailPromotor+") in verband met de bachelorproef van "+ naamStudent +" met de titel \""+ titelBp +"\"." + "\nDe vraag luidt:\n" + vraagAanBPC;
                    break;
                case "Goedgekeurd" :
                    OntvangersList.Add(emailBPC);
                    OntvangersList.Add(emailStudent);
                    Onderwerp = "Bachelorproef van " + naamStudent + " is goedgekeurd";
                    Boodschap = "De bachelorproef van "+ naamStudent+ " met de titel \""+titelBp+"\", is goedgekeurd zonder opmerkingen.";
                    break;
                case "GoedgekeurdMetOpmerking" :
                    OntvangersList.Add(emailBPC);
                    OntvangersList.Add(emailStudent);
                    Onderwerp = "Bachelorproef van " + naamStudent + " is goedgekeurd met opmerkingen";
                    Boodschap = "De bachelorproef van " + naamStudent + " met de titel \"" + titelBp + "\", is goedgekeurd maar met volgende opmerkingen:\n" + opmerking + ".";
                    break;
                case "Afgekeurd" :
                    OntvangersList.Add(emailBPC);
                    OntvangersList.Add(emailStudent);
                    Onderwerp = "Bachelorproef van " + naamStudent + " is afgekeurd";
                    Boodschap = "De bachelorproef van " + naamStudent + " met de titel \"" + titelBp + "\", is Afgekeurd en krijgt de status \"nieuw voorstel\".";
                    try
                    {
                        if (!opmerking.Equals(""))
                        {
                            Boodschap += "\nEr werden volgende opmerkingen gegeven:\n" + opmerking + ".";
                        }
                    }
                    catch (NullReferenceException e)
                    {
                        //wanneer opmerking null is moet er niets gebeuren
                    }
                    break;
            }

        }
    }
}