
using System.Collections.Generic;
using System.Linq;


namespace ProjectenII.Models.Domain
{
    
    public class Promotor : Gebruiker
    {
        public virtual ICollection<Student> Studenten { get; set; }


        public Promotor()
        {
            Studenten = new List<Student>();
        }

        public Promotor(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, Campus campus)
            : base(gebruikersNaam, naam, voornaam, email, wachtwoord, eersteAanmelding, campus)
        {
            Studenten = new List<Student>();
        }

        public Promotor(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, List<Student> studenten, Campus campus)
            : base(gebruikersNaam, naam, voornaam, email, wachtwoord, eersteAanmelding, campus)
        {
            Studenten = studenten;
        }

        public void Evalueer(string id, string vraagAanBPC, string opmerking, string likertSchaal, string welkeState, EmailHulp emailHulp)
        {
            Studenten.SingleOrDefault(b => b.GebruikersNaam == id).Voorstel.Evalueer(vraagAanBPC, opmerking, likertSchaal, welkeState, emailHulp);
        }

        

    }
}
