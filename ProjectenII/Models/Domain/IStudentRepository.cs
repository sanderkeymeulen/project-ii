﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public interface  IStudentRepository
    {
        IQueryable<Student> FindAll();
        Student FindBy(string naam);
        Student FindByPromotor(string naam);
        void SaveChanges();
    }
}