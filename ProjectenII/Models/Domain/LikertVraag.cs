﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public class LikertVraag
    {
        public string Vraag { get; set; }

        public string Value { get; set; }

        public LikertVraag()
        {

        }

        

        public LikertVraag(string vraag, string value)
        {
            this.Vraag = vraag;
            this.Value = value;
            
        }
    }
}