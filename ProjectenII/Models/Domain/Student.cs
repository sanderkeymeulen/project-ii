﻿using System.Collections.Generic;

namespace ProjectenII.Models.Domain
{

    public class Student : Gebruiker
    {

        public virtual Promotor Promotor { get; set; }

        public virtual Voorstel Voorstel { get; set; }

        public Student()
        {

        }

        public Student(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, Campus campus)
            : base(gebruikersNaam, naam, voornaam, email, wachtwoord, eersteAanmelding, campus)
        {

        }

        public Student(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, Promotor promotor, Campus campus)
            : base(gebruikersNaam, naam, voornaam, email, wachtwoord, eersteAanmelding, campus)

        {
            Promotor = promotor;
        }

        public void StelVoorstelIn(string titel, IList<Onderzoeksdomein> onderzoeksdomeinen, string trefwoorden, string probleemstelling, string onderzoeksvraag, string planVanAanpak,
            string referentielijst, string copromotorNaam, string copromotorEmail)
        {
            Voorstel = new Voorstel
            {
                Titel = titel,
                Onderzoeksdomeinen = onderzoeksdomeinen,
                Trefwoorden = trefwoorden,
                Probleemstelling = probleemstelling,
                Onderzoeksvraag = onderzoeksvraag,
                PlanVanAanpak = planVanAanpak,
                Referentielijst = referentielijst,
                CopromotorNaam = copromotorNaam,
                CopromotorEmail = copromotorEmail
            };

            Voorstel.ToNieuwVoorstelState();
        }

        public void EditVoorstel(string titel, IList<Onderzoeksdomein> onderzoeksdomeinen, string trefwoorden, string probleemstelling, string onderzoeksvraag, string planVanAanpak,
            string referentielijst, string copromotorNaam, string copromotorEmail)
        {
            Voorstel.EditVoorstel(titel, onderzoeksdomeinen, trefwoorden, probleemstelling, onderzoeksvraag, planVanAanpak,
            referentielijst, copromotorNaam, copromotorEmail);
        }

        public void DienIn()
        {
            Voorstel.DienIn();
        }

    }
}
