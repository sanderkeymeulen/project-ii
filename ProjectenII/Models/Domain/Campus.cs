﻿using System;

namespace ProjectenII.Models.Domain
{
    public class Campus
    {
        public String Naam { get; set; }

        public Campus()
        {
        }

        public Campus(String naam)
        {
            this.Naam = naam;
        }
    }
} 