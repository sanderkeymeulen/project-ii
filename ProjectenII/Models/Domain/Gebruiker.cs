﻿using System.ComponentModel.DataAnnotations;

namespace ProjectenII.Models.Domain
{
    
    public abstract class Gebruiker
    {

        [Key]
        public string GebruikersNaam { get; set; }
  
        public string Voornaam { get; set; }
        public string Naam { get; set; }
        public string Email { get; set; }
        public string Wachtwoord { get; set; }
        public bool EersteAanmelding { get; set; }
        public Campus Campus { get; set; }

        public Gebruiker()
        {

        }

        public Gebruiker(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, Campus campus)
        {
            GebruikersNaam = gebruikersNaam;
            Naam = naam;
            Voornaam = voornaam;
            Email = email;
            Wachtwoord = wachtwoord;
            EersteAanmelding = eersteAanmelding;
            Campus = campus;
        }

        
    }
}
