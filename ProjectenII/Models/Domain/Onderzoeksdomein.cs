﻿using System.ComponentModel.DataAnnotations;

namespace ProjectenII.Models.Domain
{
    public class Onderzoeksdomein
    {
        [Key]
        public int ODId { get; set; }

        public string Naam { get; set; }

        public Onderzoeksdomein(int id, string naam)
        {
            ODId = id;
            Naam = naam;
        }

        public Onderzoeksdomein()
        {
        }

    }
}
