﻿
namespace ProjectenII.Models.Domain
{
    public class BPC : Gebruiker
    {
        public BPC()
        {

        }

        public BPC(string gebruikersNaam, string naam, string voornaam, string email, string wachtwoord, bool eersteAanmelding, Campus campus)
            : base(gebruikersNaam, naam, voornaam, email, wachtwoord, eersteAanmelding, campus)
        {

        }

        public void StelAdviesIn(string advies, Student student)
        {
            student.Voorstel.StelAdviesIn(advies, student.Promotor.Email);
        }
    }
}
