﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public interface IVraagRepository
    {
        IQueryable<Vraag> FindAll();
        Vraag FindBy(int id);
    }
}