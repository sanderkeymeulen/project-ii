﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public class Vraag
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}