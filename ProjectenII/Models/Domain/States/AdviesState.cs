﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain.States
{
    class AdviesState : VoorstelState
    {
        internal AdviesState(Voorstel voorstel)
            : base(voorstel)
        {

        }

        internal override void StelAdviesIn(string advies, string email)
        {
            voorstel.Advies = advies;
            List<string> ontvangerList =new List<string>();
            ontvangerList.Add(email);
            string boodschap = "Er is advies gegeven voor voorstel :" + voorstel.Titel + ". " + advies;
            Email.stuurEmail(new EmailHulp()
            {
                Boodschap = boodschap,
                OntvangersList = ontvangerList
            });
            voorstel.ToInBehandelingState();
        }
    }
}