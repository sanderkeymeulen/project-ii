﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectenII.Models.DAL;

namespace ProjectenII.Models.Domain.States
{
    public abstract class VoorstelState
    {

        protected Voorstel voorstel;
        
        
	    protected VoorstelState(Voorstel voorstel) {
            this.voorstel = voorstel;
	    }

        internal virtual void DienIn()
        {
            throw new NotImplementedException();  
        }

        internal virtual void StelAdviesIn(string advies, string email)
        {
            throw new NotImplementedException();
        }

        internal virtual void EditVoorstel(string titel, IList<Onderzoeksdomein> onderzoeksdomeinen, string trefwoorden, string probleemstelling, string onderzoeksvraag, string planVanAanpak,
            string referentielijst, string copromotorNaam, string copromotorEmail)
        {
            throw new NotImplementedException();
        }

        internal virtual void Evalueer(string advies, string opmerking, string likertSchaal, string welkeState, EmailHulp emailHulp)
        {
            throw new NotImplementedException();
        }
    }
}