﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectenII.Models.Domain.States
{
    class InBehandelingState : VoorstelState
    {
        internal InBehandelingState(Voorstel voorstel) : base(voorstel)
        {
            
        }

        internal override void Evalueer(string vraagAanBPC, string opmerking, string likertSchaal, string welkeState, EmailHulp emailHulp)
        {
            List<string> ontvangersList;
            string bericht;
            voorstel.LikertSchaal = likertSchaal;
            switch(welkeState)
            {
                case "VraagAanBPC":
                    voorstel.VraagAanBPC = vraagAanBPC;
                    Email.stuurEmail(emailHulp);
                    voorstel.ToAdviesState();
                    break;
                case "Goedgekeurd": 
                    Email.stuurEmail(emailHulp);
                    voorstel.ToGoedgekeurdState();
                    break;
                case "GoedgekeurdMetOpmerking": 
                    voorstel.Opmerking =opmerking;
                    Email.stuurEmail(emailHulp);
                    voorstel.ToGoedgekeurdMetOpmerkingenState();
                    break;
                case "Afgekeurd": 
                    voorstel.Afkeuren();
                    voorstel.Opmerking = opmerking;
                    Email.stuurEmail(emailHulp);
                    voorstel.ToNieuwVoorstelState();
                    break;
                default: voorstel.VraagAanBPC = vraagAanBPC; 
                         voorstel.Opmerking=opmerking;
                    break;
            }
        }


    }
}