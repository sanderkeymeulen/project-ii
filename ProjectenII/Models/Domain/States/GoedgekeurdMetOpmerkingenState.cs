﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain.States
{
    public class GoedgekeurdMetOpmerkingenState : VoorstelState
    {
        public GoedgekeurdMetOpmerkingenState(Voorstel voorstel) : base(voorstel)
        {
        }
    }
}