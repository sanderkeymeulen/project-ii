﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectenII.Models.DAL;

namespace ProjectenII.Models.Domain.States
{
    class NieuwVoorstelState : VoorstelState
    {
        internal NieuwVoorstelState(Voorstel voorstel)
            : base(voorstel)
        {

        }

        internal override void EditVoorstel(string titel, IList<Onderzoeksdomein> onderzoeksdomeinen, string trefwoorden, string probleemstelling,
            string onderzoeksvraag, string planVanAanpak, string referentielijst, string copromotorNaam, string copromotorEmail)
        {
            voorstel.Titel = titel;
            voorstel.Onderzoeksdomeinen = onderzoeksdomeinen;
            voorstel.Trefwoorden = trefwoorden;
            voorstel.Probleemstelling = probleemstelling;
            voorstel.Onderzoeksvraag = onderzoeksvraag;
            voorstel.PlanVanAanpak = planVanAanpak;
            voorstel.Referentielijst = referentielijst;
            voorstel.CopromotorNaam = copromotorNaam;
            voorstel.CopromotorEmail = copromotorEmail;
        }

        internal override void DienIn()
        {
            voorstel.ToInBehandelingState();
            voorstel.IndienDatum = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }
}