﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectenII.Models.Domain
{
    public interface IGebruikerRepository
    {
        IQueryable<Gebruiker> FindAll();
        IQueryable<Gebruiker> FindAllStudenten();
        Gebruiker FindBy(string name);
        IQueryable<Gebruiker> FindAllBPCs();
        void SaveChanges();

        
    }
}