﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls.WebParts;
using MySql.Data.MySqlClient;
using ProjectenII.Models.Domain.States;


namespace ProjectenII.Models.Domain
{
    public class Voorstel
    {
        [Key]    
        public int Voorstelnummer { get; set; }

        public string Titel { get; set; }
        
        public virtual IList<Onderzoeksdomein> Onderzoeksdomeinen { get; set; }

        public string Trefwoorden { get; set; }

        public string Probleemstelling { get; set; }

        public string Onderzoeksvraag { get; set; }
      
        public string PlanVanAanpak { get; set; }

        public string Referentielijst { get; set; }

        public string CopromotorNaam { get; set; }

        public string CopromotorEmail { get; set; }

        public string Advies { get; set; }

        public string Opmerking { get; set; }

        public string VraagAanBPC { get; set; }

        public bool FeedbackFinaal { get; set; }

        public string IndienDatum { get; set; }
        
        [NotMapped]
        public IList<LikertVraag> LikertVragen  {  get; set;  }

        private string likertSchaal;

        public string LikertSchaal
        {
            get { return likertSchaal; }
            set
            {
                if (value != null)
                {
                likertSchaal = value;
                    string[] likertSchaalWaarden = LikertSchaal.Split(',');
                    LikertVragen = new List<LikertVraag>();
                    
                    foreach (string valueString in likertSchaalWaarden)
                    {
                        
                        LikertVragen.Add(new LikertVraag()
                        {
                            Value = valueString,
                            Vraag = ""
                        });
                    }
                }

            }
        }

        [NotMapped]
        private VoorstelState _currentState;

        private string voorstelStateString;

        public string VoorstelStateString
        {
            get
            {
                return voorstelStateString;
            }
            set
            {
                voorstelStateString = value;
                switch (value)
                {
                    case "Goedgekeurd": _currentState = new GoedgekeurdState(this);
                        break;
                    case "Advies": _currentState = new AdviesState(this);
                        break;
                    case "InBehandeling": _currentState = new InBehandelingState(this);
                        break;
                    case "NieuwVoorstel": _currentState = new NieuwVoorstelState(this);
                        break;
                    case "GoedgekeurdMetOpmerkingen": _currentState = new GoedgekeurdState(this);
                        break;
                    default:
                        _currentState = new NieuwVoorstelState(this);
                        break;
                }
            }
        }

            
        public Voorstel()
        {
            _currentState = ToNieuwVoorstelState();
            Onderzoeksdomeinen =  new List<Onderzoeksdomein>();
            LikertVragen = new List<LikertVraag>();
        }

        public Voorstel(string titel)
        {
            this.Titel = titel;
            Onderzoeksdomeinen = new List<Onderzoeksdomein>();
        }

        internal VoorstelState ToNieuwVoorstelState()
        {
            VoorstelStateString = "NieuwVoorstel";
            return _currentState;
        }
            
        internal VoorstelState ToAdviesState()
        {
            VoorstelStateString = "Advies";
            return _currentState;
        }

        internal VoorstelState ToInBehandelingState()
        {
            VoorstelStateString = "InBehandeling";
            return _currentState;
        }
       
        internal VoorstelState ToGoedgekeurdState()
        {
            VoorstelStateString = "Goedgekeurd";
            return _currentState;
        }

        internal VoorstelState ToGoedgekeurdMetOpmerkingenState()
        {
            VoorstelStateString = "GoedgekeurdMetOpmerkingen";
            return _currentState;
        }

        public void DienIn()
        {
            _currentState.DienIn();
        }

        public void EditVoorstel(string titel, IList<Onderzoeksdomein> onderzoeksdomeinen, string trefwoorden, string probleemstelling, string onderzoeksvraag, string planVanAanpak,
            string referentielijst, string copromotorNaam, string copromotorEmail)
        {
            _currentState.EditVoorstel(titel, onderzoeksdomeinen, trefwoorden, probleemstelling, onderzoeksvraag,
                planVanAanpak, referentielijst, copromotorNaam, copromotorEmail);
        }

        public void StelAdviesIn(string advies, string email)
        {
            _currentState.StelAdviesIn(advies, email);
        }

        public void StelVraagAanBPCIn(string vraagAanBPC)
        {
            VraagAanBPC = vraagAanBPC;
        }

        public void Evalueer(string vraagAanBPC, string opmerking, string likertSchaal, string welkeState, EmailHulp emailHulp)
        {
            _currentState.Evalueer(vraagAanBPC, opmerking, likertSchaal, welkeState, emailHulp);
        }

        public void StelOpmerkingIn(string opmerking)
        {
            this.Opmerking = opmerking;
        }

        public void Afkeuren()
        {
            Titel = null;
            Onderzoeksdomeinen = null;
            Trefwoorden = null;
            Probleemstelling = null;
            Onderzoeksvraag = null;
            PlanVanAanpak = null;
            Referentielijst = null;
            CopromotorNaam = null;
            CopromotorEmail = null;
            Advies = null;
            LikertSchaal = null;
        }

        public string[] GetLikertAntwoorden()
        {
            string[] likertSchaalWaarden = LikertSchaal.Split(',');
            return likertSchaalWaarden;
        }

        public void MaakOnderzoeksdomeinenLeegInDb()
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bachelorproef"].ConnectionString;
                MySqlConnection conn = new MySqlConnection(connString);
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM voorstelonderzoeksdomein WHERE Voorstelnummer = "+Voorstelnummer+";", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                conn.Close();
            }
            catch (MySqlException ex)
            {

            }
        }
    }
}
