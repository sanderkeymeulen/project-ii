﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;

namespace ProjectenII.Controllers
{   [Authorize(Roles = "Promotor")]
    public class EvalueerController : Controller
    {
        
        private IGebruikerRepository gebruikerRepository;
        private IVraagRepository vraagRepository;
        
        
        public EvalueerController(IGebruikerRepository gebruikerRepository, IVraagRepository vraagRepository)
        {
            this.gebruikerRepository = gebruikerRepository;
            this.vraagRepository = vraagRepository;
            IEnumerable<Student> studenten = Enumerable.Cast<Student>(gebruikerRepository.FindAllStudenten()).ToList();
        }
    

    
        // GET: /Evalueer/
    public ActionResult Index(Promotor promotor)
    {
        try
        {
            IEnumerable<Student> alleStudenten = promotor.Studenten;
            IList<Student> studenten =
                alleStudenten.Where(
                    st => st.Voorstel != null && st.Voorstel.VoorstelStateString.Equals("InBehandeling")).ToList();
            //alle studenten moet naar studenten worden veranderd wanneer je enkel die in behandeling wilt zien cfr. lastenboek
            return View(new EvalueerViewModel(studenten));
        }
        catch (Exception ex)
        {
            TempData["Melding"] = "Er is iets fout gegaan bij het opstellen van een lijst met voorstellen";
            return RedirectToAction("Index", "Evalueer");

        }
    }

    public ActionResult GeefEvaluatie(Promotor promotor, string id)
        {
            try
            {
                Student student = promotor.Studenten.SingleOrDefault(b => b.GebruikersNaam == id);
                IEnumerable<Vraag> vragen = vraagRepository.FindAll();
                if (student.Voorstel.LikertSchaal == null || student.Voorstel.LikertSchaal.Equals(""))
                {
                    foreach(Vraag vraag in vragen)
                    {
                        student.Voorstel.LikertSchaal += "middelmatig,";
                    }
                    student.Voorstel.LikertSchaal = student.Voorstel.LikertSchaal.Remove(student.Voorstel.LikertSchaal.Length - 1);
                }
                int i = 0;
                string[] antwoorden = student.Voorstel.GetLikertAntwoorden();
                student.Voorstel.LikertVragen = new List<LikertVraag>();
                foreach (Vraag vraag in vragen)
                {
                    student.Voorstel.LikertVragen.Add(new LikertVraag(vraag.Text, antwoorden[i]));
                    i++;
                }
                return View("GeefEvaluatie", new GeefEvaluatieViewModel(student));
            }
            catch (Exception ex)
            {
                TempData["Melding"] = 
                      "Er is iets fout gegaan bij het geven van de evalualtie, gelieve het nog eens opnieuw te proberen";
                return RedirectToAction("Index", "Evalueer");
               // ModelState.AddModelError("Er is iets fout gegaan bij het geven van de evalualtie, gelieve het nog eens opnieuw te proberen", ex.Message);
            }
           
        }

        [HttpPost, ActionName("GeefEvaluatie")]
        public ActionResult GeefEvaluatie([Bind(Prefix = "Voorstel")]VoorstelViewModel model, Promotor promotor, string id, string button, string vraagAanBPC)
        {
            try
            {
                string likertString = "";
                Student student = promotor.Studenten.SingleOrDefault(b => b.GebruikersNaam == id);

                foreach (var q in model.LikertVragen)
                {
                    likertString += q.Value + ",";
                }
                likertString = likertString.Remove(likertString.Length - 1);
                System.Diagnostics.Debug.Write(likertString);

                //Wanneer je 2 maal na elkaar zou evalueren zouden er velden blijven staan die nu overgeslagen zouden worden

                string welkeState = "";
                //We kijken eerst of we iets aan de bpc moeten vragen
                if (button.Equals("Evalueer"))
                {
                    if (vraagAanBPC.Equals("vraagAanBPC"))
                    {

                        welkeState = "VraagAanBPC";
                    }
                    if (vraagAanBPC.Equals("opmerking"))
                    {

                        welkeState = "GoedgekeurdMetOpmerking";
                    }
                    if (vraagAanBPC.Equals("leeg"))
                    {
                        welkeState = "Goedgekeurd";
                    }
                }

                if (button.Equals("Afkeuren"))
                {
                    welkeState = "Afgekeurd";
                }

                if (button.Equals("Save"))
                {
                    welkeState = "";
                }

                string emailBPC =
                    gebruikerRepository.FindAllBPCs().SingleOrDefault(b => b.Campus.Naam == student.Campus.Naam).Email;

                EmailHulp emailHulp = new EmailHulp((promotor.Naam + " " + promotor.Voornaam), promotor.Email,
                    student.Naam + " " + student.Voornaam, student.Email, emailBPC, model.VraagAanBPC, model.Opmerking,
                    model.Titel, welkeState);

                promotor.Evalueer(id, model.VraagAanBPC, model.Opmerking, likertString, welkeState, emailHulp );

                gebruikerRepository.SaveChanges();

               TempData["Geslaagd"] =
                    "De actie " + "\"" + button + "\" werd succesvol verwerkt";
                return RedirectToAction("Index");
            }
            catch (NotImplementedException Nie)
            {
                TempData["Melding"] = "U kan het voorstel niet meer aanpassen omdat het niet de juiste status heeft.";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Melding"] = "Er is iets fout gegaan bij het geven van de evaluatie, gelieve het nog eens opnieuw te proberen";
                return RedirectToAction("GeefEvaluatie");
            }
            
        }
	}
}