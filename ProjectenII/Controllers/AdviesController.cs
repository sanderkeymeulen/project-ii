﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;


namespace ProjectenII.Controllers
{
    [Authorize(Roles = "BPC")]
    public class AdviesController : Controller
    {

        private IGebruikerRepository gr;
        private IVraagRepository vr;

        public AdviesController(IGebruikerRepository gr, IVraagRepository vr)
        {
            this.gr = gr;
            this.vr = vr;
        }
        
        public ActionResult Index(BPC bpc)
        {
            IEnumerable<Student> alleStudenten = Enumerable.Cast<Student>(gr.FindAllStudenten()).ToList();
            alleStudenten = alleStudenten.Where(b => b.Campus.Naam.Equals(bpc.Campus.Naam));
            IList < Student > studenten = alleStudenten.Where(st => st.Voorstel != null && st.Voorstel.VoorstelStateString.Equals("Advies")).ToList();

            return View(new AdviesIndexViewModel(studenten.Select(s => s.ConvertToStudentIndexModel())));
        }


        public ActionResult GeefAdvies(string id)
        {

            var student = (Student)gr.FindBy(id);

            List<Vraag> vragen = vr.FindAll().ToList();
            int i = 0;
            foreach (var vraag in vragen)
            {
                student.Voorstel.LikertVragen[i].Vraag = vraag.Text;
                i++;
            }
            return View(new AdviesViewModel(student));
        }
        
        [HttpPost, ActionName("GeefAdvies")]
        public ActionResult DienAdviesIn(BPC bpc, string id, [Bind(Prefix = "Voorstel")]VoorstelViewModel vw)
        {
            try
            {
                var student = (Student) gr.FindBy(id);

                bpc.StelAdviesIn(vw.Advies, student);
                gr.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(new AdviesViewModel((Student) gr.FindBy(id)));
            }
        }
	}
}