﻿using ProjectenII.Models.Domain;
using System.Web.Mvc;

namespace ProjectenII.Controllers
{
    public class HomeController : Controller
    {
        //private IStudentRepository studentRepository;
        private IGebruikerRepository gebruikersRepository;

        public HomeController(IGebruikerRepository gebruikerRepository)
        {
            gebruikersRepository = gebruikerRepository;
        }

        public ActionResult Index()
        {
            return View(gebruikersRepository.FindAll());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}