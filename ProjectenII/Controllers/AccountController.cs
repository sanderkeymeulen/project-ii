﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using ProjectenII.Models;
using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;

namespace ProjectenII.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IGebruikerRepository gebruikerRepository;
        public AccountController(IGebruikerRepository gebruikerRepository)
        {
            this.gebruikerRepository = gebruikerRepository;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Gebruiker g = gebruikerRepository.FindBy(model.UserName);

                    if (g != null) {

                        if (Encrypt(model.Password).Equals(g.Wachtwoord))
                        {
                            if (g.EersteAanmelding)
                            {
                                TempData["EersteAanmelding"] =
                                    "Dag " + g.Voornaam + g.Naam +
                                    "! Je meldt voor de eerste keer aan, wijzig hier je wachtwoord in je eigen keuze.";
                                return RedirectToAction(actionName: "WijzigWachtwoord",
                                    routeValues: new {returnUrl, g.GebruikersNaam});
                            }
                            GeefRol(g, model.UserName);
                            return RedirectToAction("MijnOpties", "Account");
                        }
                    }
                }
                   
                catch (NullReferenceException e)
                {
                   ViewBag.Waarschuwing = "Fout in de gebruikersnaam of het wachtwoord. Probeer opnieuw.";
                   return View(model);
                } 
                }
            ViewBag.Waarschuwing = "Fout in de gebruikersnaam of het wachtwoord. Probeer opnieuw.";
            return View(model);
        }

        //
        // GET: /Account/WijzigWachtwoord
        [AllowAnonymous]
        public ActionResult WijzigWachtwoord(string returnUrl, string gebruikersNaam)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Gebruikersnaam = gebruikersNaam;
            return View();
        }

        //
        // POST: /Account/WijzigWachtwoord
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult WijzigWachtwoord(WijzigWachtwoordModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string gebruikersNaam = model.GebruikersNaam;
                    Gebruiker g = gebruikerRepository.FindBy(gebruikersNaam);
                    if (Encrypt(model.OldPassword).Equals(g.Wachtwoord) &&
                        model.NewPassword.Equals(model.ConfirmPassword))
                    {
                        g.Wachtwoord = Encrypt(model.NewPassword);
                        g.EersteAanmelding = false;
                        gebruikerRepository.SaveChanges();

                        GeefRol(g, gebruikersNaam);

                        return RedirectToAction("MijnOpties", "Account");
                    }
                }
            }
            catch (NullReferenceException e)
            {
                ViewBag.Waarschuwing = "Huidig wachtwoord is incorrect";
                return View(model);  
            }
            // If we got this far, something failed, redisplay form
            ViewBag.Waarschuwing = "Wachtwoorden komen niet overeen";
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        //Wachtwoord genereren en versturen via email
        [AllowAnonymous]
        public ActionResult NieuwWachtwoord()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult NieuwWachtwoord(NieuwWachtwoordModel model)
        {
            if (ModelState.IsValid)
            {
                Gebruiker g = gebruikerRepository.FindBy(model.GebruikersNaam);

                string wachtwoord = CreateRandomPassword();
                string email = g.Email;
                g.EersteAanmelding = true;
                g.Wachtwoord = Encrypt(wachtwoord);
                gebruikerRepository.SaveChanges();

                verzendEmail(wachtwoord, email);
            }
            return RedirectToAction("Login", "Account");
        }

        private void verzendEmail(string wachtwoord, string email)
        {
            List<string> ontvangersList = new List<string>();
            ontvangersList.Add(email);
            string message = string.Format("Op {0} heeft u een nieuw wachtwoord aangevraagd.\n\n", DateTime.UtcNow.Date.ToString("dd/MM/yyyy"));
            message += string.Format("Uw nieuwe wachtwoord is {0}. Gelieve hiermee in te loggen.\nDaarna kan u uw wachtwoord wijzigen naar uw keuze.", wachtwoord);

            Email.stuurEmail(new EmailHulp()
            {
                Boodschap = message,
                OntvangersList = ontvangersList,
                Onderwerp = "Wachtwoordwijziging Bachelorproef portaal"
            });
        }

        // GET: /Account/WijzigWachtwoord
        [Authorize]
        public ActionResult MijnOpties()
        {
            try
            {
                Gebruiker gebruiker = gebruikerRepository.FindBy(User.Identity.Name);
                return View(new EnkelGebruikersnaamViewModel(gebruiker));
            }
            catch (NullReferenceException e)
            {
                return View();
            }
        }

        #region Helpers
        public String Encrypt(String password)
        {
            // byte array representation of that string
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);

            // need MD5 to calculate the hash
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

            // string representation (similar to UNIX format)
            string encoded = BitConverter.ToString(hash)
                // without dashes
               .Replace("-", string.Empty)
                // make lowercase
               .ToLower();

            return encoded;
        }

        private static string CreateRandomPassword()
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[20];
            Random rd = new Random();

            for (int i = 0; i < 20; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        private void GeefRol(Gebruiker g, String gebruikersNaam)
        {
            string rol = null;
            if (g is Student)
                rol = "Student";
            else if (g is Promotor)
                rol = "Promotor";
            else if (g is BPC)
                rol = "BPC";
            //aanmaken van authenticatie ticket die in cookie komt
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, gebruikersNaam, DateTime.Now, DateTime.Now.AddMinutes(20), false, rol, "/");
            //aanmaken van cookie
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
            Response.Cookies.Add(cookie);
            HttpContext.User = new GenericPrincipal(new GenericIdentity(gebruikersNaam), new string[] { rol });
        }

        #endregion

    }
}
