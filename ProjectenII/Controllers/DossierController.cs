﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectenII.Infrastructure;
using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;

namespace ProjectenII.Controllers
{
    [Authorize(Roles = "BPC, Promotor, Student")]
    public class DossierController : Controller
    {
        private IGebruikerRepository gebruikerRepository;
        private IVraagRepository vraagRepository;
        private IEnumerable<Student> studenten;
        private IEnumerable<Vraag> vragen; 

        public DossierController(IGebruikerRepository gebruikerRepository, IVraagRepository vraagRepository)
        {
            this.gebruikerRepository = gebruikerRepository;
            this.vraagRepository = vraagRepository;
            studenten = Enumerable.Cast<Student>(gebruikerRepository.FindAllStudenten()).ToList();
            vragen = vraagRepository.FindAll();
        }

        //
        // GET: /Dossier/
        public ActionResult Index()
        {
            return View(new DossiersViewModel(studenten));
        }

        public ActionResult ToonDossier(String id)
        {
  
            Student student1 = (Student) gebruikerRepository.FindBy(id);

            if (student1.Voorstel != null)
            {
                return View("ToonDossier", new DossierViewModel(student1, vragen));
            }
            //wanneer het tot hier geraakt is er iets fout gegaan
                TempData["GeenStudent"] = student1.Voornaam + " " + student1.Naam + " heeft nog geen voorstel ingediend";
                return RedirectToAction("Index");
        }

        public ActionResult GeefEvaluatie(Promotor promotor, String id)
        {
            return RedirectToAction("GeefEvaluatie", "Evalueer", new { promotor, id });
        }

    }
}