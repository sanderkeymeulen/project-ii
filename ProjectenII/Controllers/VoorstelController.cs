﻿using System;

using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ProjectenII.Controllers
{   [Authorize(Roles = "Student")]
    public class VoorstelController : Controller
    {
        private IGebruikerRepository gr;
        private IOnderzoeksdomeinRepository or;
        
        
        public VoorstelController(IOnderzoeksdomeinRepository or, IGebruikerRepository gr)
        {
            this.or = or;
            this.gr = gr;
            
        }

       
        public ActionResult Index(Student student)
        {

            try
            {
                return View(new StudentIndexViewModel()
                {
                    ReedsVoorstel = student.Voorstel.VoorstelStateString.Equals("NieuwVoorstel") ? "nee" : "ja"
                }

                    );
            }
            catch (NullReferenceException e)
            {
                return View(new StudentIndexViewModel()
                {
                    ReedsVoorstel = "nieuw"
                });
            }

        }

        

        public ActionResult Create(Student student)
        {
            return View(student.Voorstel == null ? new CreateViewModel(or.FindAll(), new VoorstelViewModel(), student) : new CreateViewModel(or.FindAll(), student.Voorstel.ConvertToVoorstelViewModel(), student));
        }


        [HttpPost, ActionName("Create")]
        public ActionResult DienIn([Bind(Prefix = "Voorstel")]VoorstelViewModel vw, Student student, string button)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    TempData["MessageSucces"] = "Voorstel \"" + vw.Titel + "\" is succesvol opgeslagen.";

                    //System.Diagnostics.Debug.Write(domein);

                    IList<Onderzoeksdomein> onderzoeksdomeinenList =
                        vw.GeselecteerdeDomeinen.Select(id => or.FindBy(id)).ToList();

                    if (student.Voorstel == null)
                        student.StelVoorstelIn(vw.Titel, onderzoeksdomeinenList, vw.Trefwoorden, vw.Probleemstelling,
                            vw.Onderzoeksvraag, vw.PlanVanAanpak,
                            vw.Referentielijst, vw.CopromotorNaam, vw.CopromotorEmail);
                    else
                    {
                        student.Voorstel.MaakOnderzoeksdomeinenLeegInDb();
                        student.EditVoorstel(vw.Titel, onderzoeksdomeinenList, vw.Trefwoorden, vw.Probleemstelling,
                            vw.Onderzoeksvraag, vw.PlanVanAanpak,
                            vw.Referentielijst, vw.CopromotorNaam, vw.CopromotorEmail);
                    }

                    if (button != "Save")
                    {
                        student.DienIn();
                        TempData["MessageSucces"] = "Voorstel \"" + vw.Titel + "\" is succesvol ingediend.";
                    }

                    gr.SaveChanges();
                }
                catch(NotImplementedException ex)
                {
                    TempData["MessageSucces"] = "";
                    TempData["MessageFail"] = "Voorstel indienen is mislukt!";
                }
                catch (Exception ex)
                {
                    return View(new CreateViewModel(or.FindAll(), vw, student));
                }

                return RedirectToAction("Index");
            }
            return View(new CreateViewModel(or.FindAll(), vw, student));
        }

	}
}