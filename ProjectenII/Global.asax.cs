﻿using System.Security.Principal;
using System.Web.Security;
using ProjectenII.Infrastructure;
using ProjectenII.Models.DAL;
using ProjectenII.Models.Domain;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProjectenII
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
      
            Database.SetInitializer<BachelorProefContext>(new BachelorProefInitializer());
            new BachelorProefContext().Gebruikers.ToList();

            //Modelbinders
            ModelBinders.Binders.Add(typeof(Student), new StudentModelBinder());
            ModelBinders.Binders.Add(typeof(Promotor), new PromotorModelBinder());
            ModelBinders.Binders.Add(typeof(BPC), new BPCModelBinder());

        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                string[] roles = authTicket.UserData.Split(new Char[] {','});
                GenericPrincipal userPrincipal = new GenericPrincipal(new GenericIdentity(authTicket.Name), roles);
                Context.User = userPrincipal;
            }
        }
    }
}
