﻿using ProjectenII.Models.Domain;

namespace ProjectenII.ViewModels
{
    public static class Mapper
    {

        //extension method
        public static VoorstelViewModel ConvertToVoorstelViewModel(this Voorstel voorstel)
        {
            return new VoorstelViewModel()
            {
                Titel = voorstel.Titel,
                Onderzoeksdomeinen = voorstel.Onderzoeksdomeinen,
                Trefwoorden = voorstel.Trefwoorden,
                Probleemstelling = voorstel.Probleemstelling,
                Onderzoeksvraag = voorstel.Onderzoeksvraag,
                PlanVanAanpak = voorstel.PlanVanAanpak,
                Referentielijst = voorstel.Referentielijst,
                CopromotorNaam = voorstel.CopromotorNaam,
                CopromotorEmail = voorstel.CopromotorEmail,
                LikertVragen = voorstel.LikertVragen,
                VraagAanBPC = voorstel.VraagAanBPC,
                Opmerking = voorstel.Opmerking,
                
            };
        }

        public static StudentIndexViewModel ConvertToStudentIndexModel(this Student student)
        {
            return new StudentIndexViewModel()
            {
                Titel = student.Voorstel.Titel,
                Naam = student.Naam,
                Voornaam = student.Voornaam,
                Email = student.Email,
                Gebruikersnaam = student.GebruikersNaam
            };
        }
    }
}



