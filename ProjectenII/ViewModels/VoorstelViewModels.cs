﻿using Ninject.Injection;
using ProjectenII.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace ProjectenII.ViewModels
{
    public class VoorstelViewModels
    {
    }

    public class VoorstelViewModel
    {
        [Required(ErrorMessage = "{0} is verplicht")]
        public string Titel { get; set; }

        public IList<Onderzoeksdomein> Onderzoeksdomeinen { get; set; }

        [Display(Name = "Trefwoorden")]
        public string Trefwoorden { get; set; }

        [Required(ErrorMessage = "{0} is verplicht")]
        [DataType(DataType.MultilineText)]
        public string Probleemstelling { get; set; }

        [Required(ErrorMessage = "{0} is verplicht")]
        public string Onderzoeksvraag { get; set; }

        [Display(Name = "Plan van aanpak")]
        [Required(ErrorMessage = "{0} is verplicht")]
        [DataType(DataType.MultilineText)]
        public string PlanVanAanpak { get; set; }

        [Display(Name = "Referentielijst")]
        public string Referentielijst { get; set; }
        public String CopromotorNaam { get; set; }

        [EmailAddress]
        public string CopromotorEmail { get; set; }

        [Required(ErrorMessage = "Gelieve minstens 1 domein te selecteren.")]
        public List<int> GeselecteerdeDomeinen { get; set; }

        public string Advies { get; set; }

        public IList<LikertVraag> LikertVragen { get; set; }

        //string als vraag aan de BPC
        [Display(Name = "Vraag van Promotor")]
        [DataType(DataType.MultilineText)]
        public string VraagAanBPC { get; set; }

        //string in het geval goedgekeurd met opmerkingen
        [Display(Name = "Opmerkingen")]
        [DataType(DataType.MultilineText)]
        public string Opmerking { get; set; }
    }

    public class CreateViewModel
    {
        public MultiSelectList Onderzoeksdomeinen { get; set; }
        
        public VoorstelViewModel Voorstel { get; set; }
        public Student Student { get; set; }

        public CreateViewModel(IEnumerable<Onderzoeksdomein> onderzoeksdomeinen, VoorstelViewModel voorstel)
        {
            //Onderzoeksdomeinen = new SelectList(onderzoeksdomeinen);
            Onderzoeksdomeinen = new MultiSelectList(onderzoeksdomeinen);
            Voorstel = voorstel;
        }

        public CreateViewModel(IEnumerable<Onderzoeksdomein> onderzoeksdomeinen, VoorstelViewModel voorstel, Student student)
        {
            IList<int> geselecteerdeWaardes = new List<int>();
            if (voorstel.Onderzoeksdomeinen != null)
            {
                foreach (var od in voorstel.Onderzoeksdomeinen)
                {
                geselecteerdeWaardes.Add(od.ODId);
                }
            }
            
            Onderzoeksdomeinen = new MultiSelectList(onderzoeksdomeinen.Select(x=>new KeyValuePair<int,string>(x.ODId,x.Naam)), "Key", "Value", geselecteerdeWaardes);

            Voorstel = voorstel;
            Student = student;
        }
    }
}