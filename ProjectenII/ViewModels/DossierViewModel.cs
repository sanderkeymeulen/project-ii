﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using ProjectenII.Models.Domain;

namespace ProjectenII.ViewModels
{
    public class DossiersViewModel
    {
        public IEnumerable<Student> Studenten { get; set; }

        public DossiersViewModel(IEnumerable<Student> studenten)
        {
            //List<Student> studenten2 = new List<Student>();
            //foreach (Student student in studenten)
            //{
            //   DossierViewModel dvm = new DossierViewModel(student);
            //   studenten2.Add(dvm.Student);
            //}
            //Studenten = studenten2;
            Studenten = studenten;
        }
    }

    public class DossierViewModel
    {
        public Student Student { get; set; }
        public IEnumerable<Vraag> Vragen { get; set; } 

        public DossierViewModel(Student student, IEnumerable<Vraag> vragen )
        {
            Student = student;
            Vragen = vragen;
            Student.Wachtwoord = null;
            try
            {
                setState(student.Voorstel.VoorstelStateString);
            }
            catch (NullReferenceException e)
            {
                Student.Voorstel = null;
            }
        }

        private void setState(string voorstelStateString)
        {
            switch (voorstelStateString)
            {
                case "Advies":
                    Student.Voorstel.VoorstelStateString = "Advies";
                    break;
                case "InBehandeling":
                    Student.Voorstel.VoorstelStateString = "In behandeling";
                    break;
                case "NieuwVoorstel":
                    Student.Voorstel.VoorstelStateString = "Nieuw voorstel";
                    break;
                case "Goedgekeurd" :
                    Student.Voorstel.VoorstelStateString = "Goedgekeurd";
                    break;
                case "GoedgekeurdMetOpmerkingen":
                    Student.Voorstel.VoorstelStateString = "Goedgekeurd met opmerkingen";
                    break;
                default:
                    Student.Voorstel.VoorstelStateString = "Onbekend";
                    break;
            }
        }
    }
}