﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectenII.Models.Domain;

namespace ProjectenII.ViewModels
{
    public class EvalueerViewModel
    {
        

        public IEnumerable<Student> Studenten { get; set; }

        public EvalueerViewModel(IEnumerable<Student> studenten)
        {
            Studenten = studenten;
        }


    }
    public class GeefEvaluatieViewModel
{
    public Student Student { get; set; }
    public VoorstelViewModel Voorstel { get; set; }
    

    public GeefEvaluatieViewModel(Student student)
    {
            Student = student; 
            Voorstel = student.Voorstel.ConvertToVoorstelViewModel();
       
    }
}

}