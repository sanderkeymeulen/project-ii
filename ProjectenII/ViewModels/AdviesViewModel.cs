﻿using System.Collections.Generic;
using ProjectenII.Models.Domain;

namespace ProjectenII.ViewModels
{
    public class AdviesIndexViewModel
    {
        public IEnumerable<StudentIndexViewModel> Studenten { get; set; }

        public AdviesIndexViewModel(IEnumerable<StudentIndexViewModel> studenten)
        {
            Studenten = studenten;
        }  
    }
    public class EnkelGebruikersnaamViewModel
    {
        public string Voornaam { get; set; }
        public string Naam { get; set; }

        public EnkelGebruikersnaamViewModel(Gebruiker gebruiker)
        {
            Voornaam = gebruiker.Voornaam;
            Naam = gebruiker.Naam;
        }
    }
    public class StudentIndexViewModel
    {
        public string Gebruikersnaam { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public string Email { get; set; }
        public string Titel { get; set; }
        public string ReedsVoorstel { get; set; }

        //public StudentIndexViewModel(Student student)
        //{
        //    Gebruikersnaam = student.GebruikersNaam;
        //    Naam = student.Naam;
        //    Voornaam = student.Voornaam;
        //    Email = student.Email;
        //    Titel = (student.Voorstel == null ? "Nog geen voorstel" : student.Voorstel.Titel);
        //    ReedsVoorstel = (student.Voorstel == null ? "nee" : "ja");
        //}
    }

    public class AdviesViewModel
    {
        public StudentIndexViewModel Student { get; set; }
        public VoorstelViewModel Voorstel { get; set; }
        //public string Advies { get; set; }

        public AdviesViewModel(Student student)
        {
            Student = student.ConvertToStudentIndexModel();
            Voorstel = student.Voorstel.ConvertToVoorstelViewModel();
        }
    }

 
}