﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectenII.Models.Domain;

namespace ProjectenII.Tests.Controllers
{
    class DummyContext
    {
        private IList<Onderzoeksdomein> onderzoeksdomeinen;

        public DummyContext()
        {
            onderzoeksdomeinen = new List<Onderzoeksdomein>
            {
                new Onderzoeksdomein(1, "onderzoeksdomein1"),
                new Onderzoeksdomein(2, "onderzoeksdomein2"),
                new Onderzoeksdomein(3, "onderzoeksdomein3"),
                new Onderzoeksdomein(4, "onderzoeksdomein4")
            };

            promotor = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "b44fd0b673eacf773ef5dc70b0ce6e15", false, new Campus("Gent"));
            student = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "b44fd0b673eacf773ef5dc70b0ce6e15", false, promotor, new Campus("Gent"));
            student.StelVoorstelIn("Het leven van de computerbug", new Onderzoeksdomein[2], "trefwoorden",
                "probleemstelling", "onderzoeksvraag", "planVanAanpak",
                "referentielijst", "copromotorNaam", "copromotorEmail");
            student.Voorstel.LikertVragen = new LikertVraag[2];
            student.Voorstel.Advies = "advies";
            student.Voorstel.VraagAanBPC = "vraag";
            student.Voorstel.ToNieuwVoorstelState();
            student.Voorstel.VoorstelStateString = "NieuwVoorstel";
        }

        public IQueryable<Onderzoeksdomein> Onderzoeksdomeinen { get { return onderzoeksdomeinen.AsQueryable(); } }

        public Student student;

        public Promotor promotor;
    }
}
