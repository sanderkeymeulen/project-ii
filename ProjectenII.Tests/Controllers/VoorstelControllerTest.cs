﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject.Activation;
using ProjectenII.Controllers;
using ProjectenII.Models.Domain;
using ProjectenII.ViewModels;

namespace ProjectenII.Tests.Controllers
{

    [TestClass]
    public class VoorstelControllerTest
    {
        private Voorstel voorstel;
        private Student student;
        private Promotor promotor;
        private Mock<IOnderzoeksdomeinRepository> or;
        private Mock<IGebruikerRepository> gr;
        private VoorstelController controller;
        private VoorstelViewModel vvm;
        
        [TestInitialize]
        public void Initialize()
        {
            DummyContext context = new DummyContext();
            or = new Mock<IOnderzoeksdomeinRepository>();
            gr = new Mock<IGebruikerRepository>();

            or.Setup(p => p.FindAll()).Returns(context.Onderzoeksdomeinen);

            controller = new VoorstelController(or.Object, gr.Object);

            student = context.student;

            vvm = student.Voorstel.ConvertToVoorstelViewModel();
            vvm.GeselecteerdeDomeinen = new List<int>(){1, 2};

        }

        //[TestMethod]
        //public void CreateMustUseConventionToChooseView()
        //{
        //    ViewResult result = controller.Create(student) as ViewResult;
        //    Assert.IsNotNull(result);
        //    Assert.IsTrue(String.IsNullOrEmpty(result.ViewName));
        //}


        //[TestMethod]
        //public void CreateMustAddVoorstelToStudent()
        //{
        //    RedirectToRouteResult result = controller.DienIn(vvm, student, "DienIn") as RedirectToRouteResult;
        //    Assert.AreEqual("Het leven van de computerbug", student.Voorstel.Titel);
        //}

    }
}
