﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectenII.Models.Domain;

namespace ProjectenII.Tests.Models
{   
    [TestClass]
    public class VoorstelTest
    {
        private Voorstel voorstel;
        private Student student;
        private Promotor promotor;

        
        
        [TestInitialize]
        public void Initialize()
        {
            voorstel = new Voorstel{Titel = "test"};
            promotor = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "b44fd0b673eacf773ef5dc70b0ce6e15", false, new Campus("Gent"));
            student = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "b44fd0b673eacf773ef5dc70b0ce6e15", false, promotor, new Campus("Gent"));
            student.Voorstel = voorstel;

        }

        [TestMethod]
        public void IndienenMoetStateVeranderen()
        {
            student.DienIn();
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "InBehandeling");
        }

        [TestMethod]
        public void AdviesAanvragenMoetStateVeranderen()
        {
            voorstel.ToInBehandelingState();
            voorstel.Evalueer("vraagAanBPCString", "opmerking", "likertSchaal", "VraagAanBPC", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "Advies");
            Assert.AreEqual(student.Voorstel.VraagAanBPC, "vraagAanBPCString");
            
        }

        [TestMethod]
        public void GoedkeurenMoetStateVeranderen()
        {
            voorstel.ToInBehandelingState();
            voorstel.Evalueer("vraagAanBPC", "opmerking", "likertSchaal", "Goedgekeurd", new EmailHulp()); 
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "Goedgekeurd");
        }

        [TestMethod]
        public void GoedkeurenMetOpmerkingenMoetStateVeranderen()
        {
            voorstel.ToInBehandelingState();
            voorstel.Evalueer("vraagAanBPC", "opmerking", "likertSchaal", "GoedgekeurdMetOpmerking", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "GoedgekeurdMetOpmerkingen");
            Assert.AreEqual(student.Voorstel.Opmerking, "opmerking");
        }

        [TestMethod]
        public void AfkeurenMoetStateVeranderen()
        {
            voorstel.ToInBehandelingState();
            voorstel.Evalueer("vraagAanBPC", "opmerking", "likertSchaal", "Afgekeurd", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "NieuwVoorstel");
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void EvaluerenInFouteStateWerptException()
        {
            voorstel.Evalueer("vraagAanBPC", "opmerking", "likertSchaal", "Afgekeurd", new EmailHulp());
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void IndienenInFouteStateWerptException()
        {
            voorstel.ToInBehandelingState();
            student.DienIn();
        }
    }
}
