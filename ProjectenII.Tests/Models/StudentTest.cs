﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectenII.Models.Domain;

namespace ProjectenII.Tests.Models
{
    [TestClass]
    public class StudentTest
    {
        private Voorstel voorstel;
        private Student student;
        private Promotor promotor;



        [TestInitialize]
        public void Initialize()
        {
            
            promotor = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "b44fd0b673eacf773ef5dc70b0ce6e15", false, new Campus("Gent"));
            student = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "b44fd0b673eacf773ef5dc70b0ce6e15", false, promotor, new Campus("Gent"));
            
            

        }

        [TestMethod]
        public void DienInTest()
        {
            voorstel = new Voorstel { Titel = "test" };
            student.Voorstel = voorstel;
            student.DienIn();
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "InBehandeling");
        }

        [TestMethod]
        public void EditVoorstelTest()
        {
            voorstel = new Voorstel { Titel = "test" };
            student.Voorstel = voorstel;
            student.EditVoorstel("nieuweTitel", null, "trefwoorden", "probleemstelling", "vraag", "plan", "reflijst", "NieuweCopromotor", "email");
            Assert.AreEqual(student.Voorstel.Titel, "nieuweTitel");
            Assert.AreEqual(student.Voorstel.CopromotorNaam, "NieuweCopromotor");
        }

        [TestMethod]
        public void StelVoorstelInTest()
        {
            student.StelVoorstelIn("nieuwVoorstel", null, "trefwoorden", "probleemstelling", "vraag", "plan", "reflijst", "copromotor", "email");
            Assert.AreEqual(student.Voorstel.Titel, "nieuwVoorstel");
            Assert.AreEqual(student.Voorstel.Probleemstelling, "probleemstelling");
        }
       

        
    }
}
