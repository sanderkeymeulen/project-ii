﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectenII.Models.Domain;

namespace ProjectenII.Tests.Models
{
    [TestClass]
    public class BPCTest
    {
        private Voorstel voorstel;
        private Student student;
        private Promotor promotor;
        private BPC pbc;



        [TestInitialize]
        public void Initialize()
        {
            voorstel = new Voorstel { Titel = "test" };
            voorstel.ToAdviesState();
            promotor = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "b44fd0b673eacf773ef5dc70b0ce6e15", false, new Campus("Gent"));
            student = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "b44fd0b673eacf773ef5dc70b0ce6e15", false, promotor, new Campus("Gent"));
            student.Voorstel = voorstel;
            
            pbc = new BPC();

        }

        [TestMethod]
        public void StelAdviesInTest()
        {
            pbc.StelAdviesIn("Dit is advies", student);
            Assert.AreEqual(student.Voorstel.Advies, "Dit is advies");
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "InBehandeling");
        }


    }
}
