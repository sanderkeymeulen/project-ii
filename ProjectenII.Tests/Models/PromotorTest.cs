﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectenII.Models.Domain;

namespace ProjectenII.Tests.Models
{
    [TestClass]
    public class PromotorTest
    {
        private Voorstel voorstel;
        private Student student;
        private Promotor promotor;



        [TestInitialize]
        public void Initialize()
        {
            voorstel = new Voorstel { Titel = "test" };
            promotor = new Promotor("12345kam", "Marks", "Karl", "glenntecqmenne@hotmail.com", "b44fd0b673eacf773ef5dc70b0ce6e15", false, new Campus("Gent"));
            student = new Student("12345bp", "Peeters", "Bert", "BertPeeters@student.hogent.be", "b44fd0b673eacf773ef5dc70b0ce6e15", false, promotor, new Campus("Gent"));
            student.Voorstel = voorstel;
            student.Voorstel.ToInBehandelingState();
            promotor.Studenten.Add(student);

        }

        [TestMethod]
        public void AdviesAanvragenTest()
        {
            promotor.Evalueer("12345bp", "Ik heb een vraag", null, "test,test", "VraagAanBPC", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "Advies");
            Assert.AreEqual(student.Voorstel.VraagAanBPC, "Ik heb een vraag");
        }

        [TestMethod]
        public void GoedkeurenMetOpmerkingTest()
        {
            promotor.Evalueer("12345bp", "vraagAanBPC", "opmerking", "likertSchaal", "GoedgekeurdMetOpmerking", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "GoedgekeurdMetOpmerkingen");
            Assert.AreEqual(student.Voorstel.Opmerking, "opmerking");
        }

        [TestMethod]
        public void AfkeurenTest()
        {
            promotor.Evalueer("12345bp", "vraagAanBPC", "opmerking", "likertSchaal", "Afgekeurd", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "NieuwVoorstel");
            Assert.IsNull(student.Voorstel.Titel);
        }

        [TestMethod]
        public void GoedkeurenTest()
        {
            promotor.Evalueer("12345bp", "vraagAanBPC", "opmerking", "likertSchaal", "Goedgekeurd", new EmailHulp());
            Assert.AreEqual(student.Voorstel.VoorstelStateString, "Goedgekeurd");
        }
        
    }
}
